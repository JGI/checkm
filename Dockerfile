FROM ubuntu:16.04 
#FROM debian:jessie

LABEL Maintainer Shijie Yao, syao@lbl.gov
# REF : https://github.com/Ecogenomics/CheckM/wiki/Installation

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# install the needed programs
ENV PACKAGES wget unzip
RUN apt-get update && apt-get install --yes ${PACKAGES}
RUN apt-get -y install python-pip

# create download and move into it
WORKDIR /download

# download and install numpy and checkm 
RUN pip install --upgrade pip 
RUN pip install numpy
RUN pip install checkm-genome 

# install hmmer
ENV SRC http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz 
ENV TARFILE hmmer-3.1b2-linux-intel-x86_64.tar.gz 
RUN wget ${SRC} && \
	tar -zxvf ${TARFILE} && \
	rm -rf ${TARFILE} && \
	mv hmmer-3.1b2-linux-intel-x86_64/binaries/* /usr/bin/. && \
	rm -rf hmmer-3.1b2-linux-intel-x86_64 

# install prodigal
RUN wget https://github.com/hyattpd/Prodigal/releases/download/v2.6.3/prodigal.linux && \
	chmod 755 prodigal.linux  && \
	mv prodigal.linux /usr/bin/prodigal

# install pplacer 
RUN wget https://github.com/matsen/pplacer/releases/download/v1.1.alpha19/pplacer-linux-v1.1.alpha19.zip && \
	unzip pplacer-linux-v1.1.alpha19.zip && \
	rm -rf pplacer-linux-v1.1.alpha19.zip && \
	mv pplacer-Linux-v1.1.alpha19/* /usr/bin/. && \
	rm -rf pplacer-Linux-v1.1.alpha19


# install checkm required files
#ENV TAR=checkm_data_v1.0.7.tar
# 20180622 : update
ENV TAR=checkm_data_2015_01_16.tar
RUN wget https://data.ace.uq.edu.au/public/CheckM_databases/${TAR}.gz && \
	tar -zxvf ${TAR}.gz && \
	rm -rf ${TAR}.gz  && \
	yes /download | checkm data setRoot

# be sure checkm files up to date
#RUN pip install checkm-genome --upgrade --no-deps
#RUN yes /download | checkm data update 


