# CheckM docker image #

CheckM depends on hmm / prodigal / pplacer and disk files (~1.6G in size)
This image includes all the programs and disk files CheckM needs.
checkm executable is in program search path. To run:

docker run --rm checkm:last checkm ARGS

